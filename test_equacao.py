import equacao_quadratica

def test_equacao():
  assert equacao_quadratica.equacao(10,1,3,2) == 132
  assert equacao_quadratica.equacao(1,1,3,2) == 6

def test_zero_linha():
  assert equacao_quadratica.equacao(-1,1,3,2) == 0

def test_zero_dois_linha():
  assert equacao_quadratica.equacao(-2,1,3,2) == 0

def test_equacao_x_negativo():
  assert equacao_quadratica.equacao(-3,1,3,2) == 2
  assert equacao_quadratica.equacao(-5,1,3,2) == 12

