def raizes(a,b,c):
  delta = b**2 - 4*a*c
  if (delta < 0):
    print("Não existe solução nos números reais")
    
  x_linha = (-b + delta**(0.5)) / (2*a)
  x_duas_linha = (-b - delta**(0.5)) / (2*a)
  return x_linha, x_duas_linha

def equacao(x,a,b,c):
  return (a*(x**2) + b*x + c)

def main():
  a = 1
  b = 3 
  c = 2
  # print("As raízes da euqação {}x² + {}x + {} = 0 são: {}".format(a,b,c, equacao(a,b,c)))
  x = int(input("Informe o valor de x: "))
  print("O resultado da função {}*{}² + {}*{} + {} é: {}".format(a,x,b,x,c,equacao(x,a,b,c)))

if __name__ == "__main__":
  main()