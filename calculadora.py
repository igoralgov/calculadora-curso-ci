import math

def add(n_1,n_2):
    return n_1 + n_2

def div(n_1,n_2):
    return int(n_1 / n_2)

def fat(num):
    res = 1
    for n in range(1,num + 1):
        res *= n
    return res

def order(lista):
    lista.sort()
    return lista

def log(n_1,n_2):
    try:
        if n_1 <= 0 or n_2 <= 0:
            return "Erro: O logaritmo só é definido para números positivos."
        result = math.log(n_1, n_2)
        return result
    except Exception as e:
        return f"Erro: {str(e)}"

def subAll(lista):
    res = 0
    for n in lista:
        res -= n
    return res

def main():
    op = 'Y'
    while(op == 'Y'):
        print('''
        Digite a função que gostaria de fazer:

        1. Adição
        2. Ordernar uma lista de forma crescente
        3. Logaritmo
        4. Fatorial
        5. Subtrair todos os elementos de uma lista
        6. Divisão 
                ''')
        operation = int (input('Opção: '))

        if(operation == 1):
            n_1 = int(input('Selecione o primeiro número: '))
            n_2 = int(input('Selecione o segundo número: '))
            print('{} + {} = {}'.format(n_1, n_2, add(n_1,n_2) ))
        elif(operation == 2):
            lista = []
            x = int(input('Digite um número para adicionar na lista (digite 0 para encerrar): '))
            lista.append(x)
            while(x != 0):
                x = int(input('Digite um número para adicionar na lista (digite 0 para encerrar): '))
                lista.append(x)
            print('A lista ordenada é: ', order(lista))
        elif(operation == 3):
            n_1 = int(input('Selecione o primeiro número: '))
            n_2 = int(input('Selecione o segundo número: '))
            print('O logarítmo de {} na base {} é: {}'.format(n_1,n_2,log(n_1,n_2)))
        elif(operation == 4):
            num = int(input('Digite o número que quer calcular o fatorial: '))
            print('O fatorial de {} é: {}'.format(num,fat(num)))
        elif(operation == 5):
            lista = []
            x = int(input('Digite um número para adicionar na lista (digite 0 para encerrar): '))
            lista.append(x)
            while(x != 0):
                x = int(input('Digite um número para adicionar na lista (digite 0 para encerrar): '))
                lista.append(x)
            print('A subtração total dos números da lista é: {}'.format(subAll(lista)))
        elif(operation == 6):
            n_1 = int(input('Selecione o primeiro número: '))
            n_2 = int(input('Selecione o segundo número: '))
            print('{} / {} = {}'.format(n_1, n_2, div(n_1,n_2) ))
        else:
            print('Operação Inválida')
        op =  input("Deseja fazero outra operação (Y/N): ")
        op = op.upper()

if __name__ == "__main__":
  main()