import math
import pytest
from calculadora import add, div, fat, order, log, subAll

# Testando a função add
def test_add():
    assert add(3, 4) == 7
    assert add(-1, 1) == 0
    assert add(0, 0) == 0

# Testando a função div
def test_div():
    assert div(8, 2) == 4
    assert div(5, 2) == 2
    with pytest.raises(ZeroDivisionError):
        div(1, 0)

# Testando a função fat
def test_fat():
    assert fat(0) == 1
    assert fat(5) == 120
    assert fat(10) == 3628800

# Testando a função order
def test_order():
    assert order([3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5]) == [1, 1, 2, 3, 3, 4, 5, 5, 5, 6, 9]
    assert order([]) == []

# Testando a função log
def log(n_1, n_2):
    if n_1 <= 0 or n_2 <= 0:
        raise ValueError("O logaritmo só é definido para números positivos.")
    try:
        result = int(math.log(n_1, n_2))
        return result
    except Exception as e:
        raise ValueError(f"Erro: {str(e)}")



# Testando a função subAll
def test_log():
    assert log(8, 2) == 3
    assert log(16, 2) == 4
    with pytest.raises(ValueError, match="O logaritmo só é definido para números positivos."):
        log(-1, 2)




